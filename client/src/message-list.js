import React from "react";
import { Comment, Header } from "semantic-ui-react";

class MessageList extends React.Component {
  render() {
    return (
      <Comment.Group>
        <Header as="h3" dividing>
          Comments
        </Header>
        {this.props.messages.map((message, index) => {
          return (
            <Comment key={index}>
              <Comment.Content>
                <Comment.Author as="a">{message.name}</Comment.Author>
                <Comment.Metadata>
                  <div>{message.time}</div>
                </Comment.Metadata>
                <Comment.Text>{message.message}</Comment.Text>
              </Comment.Content>
            </Comment>
          );
        })}
      </Comment.Group>
    );
  }
}

export default MessageList;
