import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import { Container, Header } from "semantic-ui-react";
import MessageForm from "./message-form";
import MessageList from "./message-list";
import "./App.css";

let socket = io(`http://localhost:5000`);

function App() {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    fetch("/messages")
      .then(res => res.json())
      .then(data => setMessages(data))
      .catch(() => {
        console.log("error");
      });
  }, []);

  useEffect(() => {
    socket.on("message", function(message) {
      setMessages([...messages, message]);
    });
  }, [messages]);

  return (
    <div className="app-container">
      <Container text>
        <Header as="h3">Message Board</Header>
        <MessageForm />
        <MessageList messages={messages} />
      </Container>
    </div>
  );
}

export default App;
