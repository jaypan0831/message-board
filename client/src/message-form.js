import React from "react";
import { Form, TextArea, Button } from "semantic-ui-react";

const inputObj = {};

class MessageForm extends React.Component {
  handleSubmit(e) {
    e.preventDefault();
    fetch("/messages", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(inputObj)
    })
      .then(res => {
        console.log(res.status);
      })
      .catch(error => console.log(error));
  }

  handleChange(e) {
    let currentDatetime = new Date();
    let formattedDate = `${currentDatetime.getFullYear()}/${currentDatetime.getMonth() +
      1}/${currentDatetime.getDate()}`;
    inputObj[e.target.name] = e.target.value;
    inputObj["time"] = formattedDate;
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit.bind(this)}>
        <Form.Field onChange={this.handleChange.bind(this)}>
          <label>Your Name</label>
          <input placeholder="Name" name="name" />
        </Form.Field>
        <Form.Field
          control={TextArea}
          label="Message"
          name="message"
          placeholder="Leave a Message"
          onChange={this.handleChange.bind(this)}
        />
        <Form.Field control={Button}>Submit</Form.Field>
      </Form>
    );
  }
}

export default MessageForm;
