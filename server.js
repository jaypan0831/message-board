const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const mongoose = require("mongoose");
const port = 5000;

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

let dbUrl =
  "mongodb+srv://user:user@cluster0-9lua8.mongodb.net/chatroom?retryWrites=true";

let Message = mongoose.model("message", {
  name: String,
  message: String,
  time: String
});

app.get("/messages", (req, res) => {
  Message.find({}, (err, messages) => {
    res.send(messages);
  });
});

app.post("/messages", async (req, res) => {
  try {
    let message = new Message(req.body);
    let saveMessage = await message.save();

    console.log("saved");
    io.emit("message", req.body);
    res.sendStatus(200);
  } catch (err) {
    res.sendStatus(500);
    return console.log(err);
  }
});

io.on("connection", socket => {
  console.log("a user connected");
});

mongoose.connect(dbUrl, { useNewUrlParser: true }, err => {
  console.log("mongo db connection", err);
});

http.listen(port, () => `server running on port ${port}`);
